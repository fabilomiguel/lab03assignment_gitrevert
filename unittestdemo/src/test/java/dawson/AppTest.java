package dawson;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class AppTest 
{
    @Test
    public void echo_returnCorrectValue()
    {
        assertEquals("Test if the method echo(int x) is matching the values properly", 5, App.echo(5));
    }

    @Test
    public void oneMore_returnCorrectValue()
    {
        assertEquals("Test if the method is adding 1 to the value of the parameter", 9, App.oneMore(8));
    }
}
